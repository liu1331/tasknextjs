import {
  createContext,
  useState,
  useContext,
  ReactNode,
  useEffect,
} from "react";
interface IAuthContext {
  user: IAuth;
  setUser: (arg: IAuth) => void;
  logout: () => void;
}
const AuthContext = createContext<IAuthContext>({
  user: {
    User: { login: "", id: "", img: "", socialNetwork: {} },
    isAuth: false,
  },
  setUser: () => {},
  logout: () => {},
});
type Props = {
  children: ReactNode;
};
interface IAuth {
  User: Object | null;
  isAuth: boolean;
}

export const AuthProvider = (props: Props) => {
  const [user, setUser] = useState<IAuth>({
    User: null,
    isAuth: false,
  });
  const [isLoading, setisLoading] = useState<boolean>(true);
  const logout = () => {
    localStorage.removeItem("token");
    setUser({
      User: null,
      isAuth: false,
    });
  };
  useEffect(() => {
    const token = localStorage.getItem("token");
    async function checkUser() {
      const response = await fetch(`/api/me`, {
        method: "GET",
        headers: {
          authorization: token ? token : "",
        },
        cache: "no-store",
      });
      if (!response.ok) {
        if (response.status === 403) {
          localStorage.removeItem("token");
        }
        return false;
      }
      const user = await response.json();
      return user;
    }
    checkUser().then((res) => {
      if (res) {
        setUser((prev) => ({ ...prev, User: { ...res }, isAuth: true }));
      }
      setisLoading(false);
    });
  }, []);
  if (isLoading) return null;
  return (
    <AuthContext.Provider value={{ user, setUser, logout }}>
      {props.children}
    </AuthContext.Provider>
  );
};
export const useAuthContext = () => useContext(AuthContext);
