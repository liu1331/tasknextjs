import { GetServerSideProps, GetStaticPaths, GetStaticProps } from "next/types";
import React from "react";
import styles from "./../styles/ArticlePage.module.scss";
import { getFormatDate } from "../functions/getFormatDate";
import { IArticle } from "./api/articles/[id]";
interface IArticlePage {
  article: IArticle;
}

const ArticlePage = ({ article }: IArticlePage) => {
  const { monthName, day, year } = getFormatDate(new Date(article.date));
  return (
    <div className={styles.articlePage}>
      <div className={styles.articlePage_title}>
        <h5>{article.title}</h5>
      </div>
      <div className={styles.articlePage_image}>
        <img src={article.img} alt="" />
      </div>
      <div className={styles.articlePage_date}>
        <p>{`${monthName} ${day}, ${year}`}</p>
      </div>
      <div className={styles.articlePage_text}>{article.body}</div>
    </div>
  );
};

export default ArticlePage;

export const getServerSideProps: GetServerSideProps = async function (context) {
  const { id } = context.query;

  const result = await fetch(`${process.env.API_URL}/articles/${id}`);
  const { data } = await result.json();
  if (!data) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      article: data,
    },
  };
};
