import { GetServerSideProps, GetStaticProps } from "next";
import Router from "next/router";
import React, { useState } from "react";
import { useAuthContext } from "../hooks/UserContext";
import styles from "../styles/LoginPage.module.scss";

const LoginPage = () => {
  const [login, setLogin] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const { setUser } = useAuthContext();
  const onClick = async () => {
    const { user, token } = await fetch(`/api/logIn`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json;charset=utf-8",
      },
      body: JSON.stringify({ login, password }),
    })
      .then((res) => res.json())
      .then(function (resJson) {
        return resJson;
      });
    if (token) {
      localStorage.setItem("token", token);
      setUser({ User: user, isAuth: true });
      Router.push("/");
    }
  };

  return (
    <div className={styles.loginPage}>
      <input
        name="login"
        type="text"
        value={login}
        onChange={(e) => setLogin(e.target.value)}
      />
      <input
        name="password"
        type="password"
        value={password}
        onChange={(e) => setPassword(e.target.value)}
      />
      <button onClick={() => login && password && onClick()}>click</button>
    </div>
  );
};

export default LoginPage;
export const getServerSideProps: GetServerSideProps = async function ({
  req,
  res,
}) {
  const response = await fetch(`${process.env.API_URL}/me`);
  if (response.ok) {
    return {
      redirect: {
        destination: "/",
        permanent: false,
      },
    };
  }

  return {
    props: { data: null },
  };
};
