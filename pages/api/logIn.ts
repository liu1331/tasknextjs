import type { NextApiRequest, NextApiResponse } from "next";
import users from "../../data/users.json";
import jwt, { Secret } from "jsonwebtoken";
type Data = {
  data: IUsers | undefined;
  token: string;
};
export interface IUsers {
  id: string;
  login: string;
  img: string;
  socialNetworks: Object;
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data | string>
) {
  const { login, password } = req.body;

  const checkedUser = users.users.find(
    (user) => user.login === login && user.password === password
  );
  if (!checkedUser) {
    res.status(403).json("User not found");
    return;
  }
  const secret = process.env.TOKEN_KEY;
  const token = jwt.sign({ login, password }, secret as Secret, {
    expiresIn: "2h",
  });
  res.status(200).json({
    data: {
      login: checkedUser?.login,
      id: checkedUser?.id,
      img: checkedUser?.img,
      socialNetworks: checkedUser?.socialNetworks,
    },
    token,
  });
}
