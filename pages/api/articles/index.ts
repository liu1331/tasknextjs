import type { NextApiRequest, NextApiResponse } from "next";
import articles from "../../../data/articles.json";
type Data = {
  articles: IArticle[];
};
export interface IArticle {
  userId: string | number;
  img: string;
  id: string | number;
  title: string;
  body: string;
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data>
) {
  res.status(200).json(articles);
}
