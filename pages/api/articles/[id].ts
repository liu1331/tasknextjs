import type { NextApiRequest, NextApiResponse } from "next";
import articles from "../../../data/articles.json";
type Data = {
  data: IArticle;
};
export interface IArticle {
  userId: string | number;
  img: string;
  id: string | number;
  title: string;
  body: string;
  date: string;
}

export default function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data | string>
) {
  const { id } = req.query;
  if (id) {
    const articleById = articles.articles.find(
      (article) => String(article.id) === id
    );
    if (articleById) res.status(200).json({ data: articleById });
  } else {
    res.status(404).json("Not found query");
  }
}
