import type { NextApiRequest, NextApiResponse } from "next";
import users from "../../data/users.json";
import jwt, { Secret } from "jsonwebtoken";
type Data = {
  data: IUsers | undefined;
};
export interface IUsers {
  id: string;
  login: string;
  img: string;
  socialNetworks: Object;
}
interface IDecodedToken {
  login: string;
  password: string;
  iat: number;
  exp: number;
}

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<Data | string>
) {
  const secret = "123";
  const { authorization } = req.headers;
  if (authorization) {
    const decode = (): Promise<IDecodedToken> =>
      new Promise((resolve, reject) => {
        jwt.verify(
          authorization,
          process.env.TOKEN_KEY as Secret,
          function (err, decodedToken) {
            if (err) {
              reject("Failed to authenticate token." + err);
            } else {
              resolve(decodedToken);
            }
          }
        );
      });
    const decodedToken: IDecodedToken | void = await decode().catch((error) =>
      res.status(401).json(`${error}`)
    );

    const currentUser = users.users.find((user) => {
      return decodedToken && decodedToken.login === user.login;
    });
    if (currentUser) {
      res.status(200).json({
        data: {
          login: currentUser.login,
          id: currentUser.id,
          img: currentUser.img,
          socialNetworks: currentUser.socialNetworks,
        },
      });
    }
  } else {
    res.status(401).json(`authorization not found`);
  }
}
