import React from "react";
import { GetStaticProps } from "next";
import Article from "../components/Article/Article";
import styles from "../styles/Blog.module.scss";

interface IBlogProps {
  userId: number | string;
  img: string;
  id: number | string;
  title: string;
  body: string;
}
interface IProps {
  articles: IBlogProps[];
}

const Blog = ({ articles }: IProps) => {
  return (
    <div className={`${styles.articles}`}>
      {articles.map((article) => {
        return (
          <Article
            key={article.id}
            articleId={article.id}
            imgSrc={article.img}
            title={article.title}
            className={styles.articleGridRowSize}
          />
        );
      })}
    </div>
  );
};

export default Blog;

export const getStaticProps: GetStaticProps = async (context) => {
  const result = await fetch(`${process.env.API_URL}/articles`);
  const { articles } = await result.json();

  if (!articles) {
    return {
      notFound: true,
    };
  }
  return {
    props: {
      articles,
    },
  };
};
