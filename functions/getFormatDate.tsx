interface IGetFormatDate {
  year: string;
  month: string;
  monthName: string;
  day: string;
  hours: string;
  minutes: string;
}

export function getFormatDate(date: Date): IGetFormatDate {
  const newDate = new Date(date);

  return {
    year: newDate.getFullYear().toString(),
    month: newDate.getMonth().toString(),
    monthName: newDate.toLocaleString("eng", { month: "short" }),
    day: newDate.toLocaleString("eng", { day: "2-digit" }),
    hours: newDate.getHours().toString(),
    minutes: newDate.getMinutes().toString(),
  };
}
