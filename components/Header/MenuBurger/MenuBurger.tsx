import React, { FC } from "react";
import styles from "./menuBurger.module.scss";
interface IMenuBurgerProps {
  isOpen?: boolean;
  setIsOpen?: (isOpen: ((v: boolean) => boolean) | boolean) => void | boolean;
}
const MenuBurger: FC<IMenuBurgerProps> = ({ isOpen = false, setIsOpen }) => {
  return (
    <div
      onFocus={() => setIsOpen && setIsOpen(false)}
      onClick={() => setIsOpen && setIsOpen((prevState) => !prevState)}
      className={`${styles.menuBurger}`}
    >
      <div />
      <div />
      <div />
    </div>
  );
};
export default MenuBurger;
