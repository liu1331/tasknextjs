import Link from "next/link";
import { useRouter } from "next/router";
import React, { FC, useEffect, useState } from "react";
import { useAuthContext } from "../../hooks/UserContext";
import { ILink, navLinks } from "../../navRoutes/nav";
import styles from "./header.module.scss";
import Logo from "./Logo/Logo";
import MenuBurger from "./MenuBurger/MenuBurger";

const Header: FC = () => {
  const { user, logout } = useAuthContext();
  const router = useRouter();
  const [isOpenMenu, setisOpenMenu] = useState<boolean>(false);
  useEffect(() => {
    if (isOpenMenu) {
      setisOpenMenu(false);
    }
  }, [router.pathname]);

  const createNavLink = (link: ILink, index: number) => {
    return {
      navLink: (
        <Link key={index} href={`${link.pathname}`}>
          <a
            className={`${
              router.pathname === link.pathname ? styles.active : ""
            }`}
          >
            {link.title}
          </a>
        </Link>
      ),
    };
  };
  return (
    <div className={styles.header}>
      <div className={styles.logo}>
        <Logo />
      </div>
      <MenuBurger setIsOpen={setisOpenMenu} isOpen={isOpenMenu} />
      <div
        className={`${styles.navigation} ${styles.nav_burger} ${
          isOpenMenu ? styles.isOpen : ""
        } `}
      >
        {user.isAuth
          ? navLinks.privateLinks.map(
              (link, index) => createNavLink(link, index).navLink
            )
          : navLinks.publicLinks.map((link, index) =>
              createNavLink(link, index)
            )}
        {user.isAuth ? (
          <Link key={"a"} href={"/"}>
            <a onClick={() => logout()}>log out</a>
          </Link>
        ) : (
          <Link key={"b"} href={"/login"}>
            <a
              className={`${router.pathname === "/login" ? styles.active : ""}`}
            >
              log in
            </a>
          </Link>
        )}
      </div>
    </div>
  );
};
export default Header;
