import Image from "next/image";
import { useRouter } from "next/router";
import React, { FC } from "react";
import styles from "./article.module.scss";
interface IArticle {
  imgSrc: string;
  articleId?: number | string;
  title?: string;
  className?: string;
}

const Article: FC<IArticle> = ({ imgSrc, title, className, articleId }) => {
  const router = useRouter();

  return (
    <div
      onClick={() => router.push(`/${articleId}`)}
      className={`${styles.article} ${className}`}
    >
      <div className={styles.image}>
        <Image src={imgSrc} alt="" layout="fill" />
      </div>
      <div className={styles.title}>
        <p>{title}</p>
      </div>
    </div>
  );
};

export default Article;
