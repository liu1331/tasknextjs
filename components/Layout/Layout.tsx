import React, { FC, ReactNode } from "react";
import Header from "../Header/Header";
import styles from "./layout.module.scss";

const Layout: FC<ReactNode> = ({ children }) => {
  return (
    <div className={`${styles.layout}`}>
      <Header />
      <div className={styles.layout_container}>{children}</div>
    </div>
  );
};

export default Layout;
