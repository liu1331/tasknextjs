interface INavLinks<T> {
  publicLinks: Array<T>;
  privateLinks: Array<T>;
}
export interface ILink {
  pathname: string;
  title: string;
  query?: string;
}

export const navLinks: INavLinks<ILink> = {
  publicLinks: [
    { pathname: "/", title: "blog", query: "[id]" },
    { pathname: "/about", title: "about" },
    { pathname: "/links", title: "links" },
    { pathname: "/projects", title: "projects" },
  ],
  privateLinks: [
    { pathname: "/", title: "blog" },
    { pathname: "/about", title: "about" },
    { pathname: "/links", title: "links" },
    { pathname: "/projects", title: "projects" },
    { pathname: "/createArticle", title: "articles" },
  ],
};
